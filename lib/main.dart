import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:visa_by_masters_hybrid/src/common/routes.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/mainpage.dart';

void main() async {
  dynamic configData;
  WidgetsFlutterBinding.ensureInitialized();
  await rootBundle.loadString('Config.json').then((val) => {
        configData = json.decode(val),
        Constants.baseUrl = configData['baseUrl'],
        Constants.imageBaseUrl = configData['imageBaseUrl']
      });
  runApp(VisaByMastersWebApp());
}

class VisaByMastersWebApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        onGenerateRoute: Router.generateRoute,
        initialRoute: Constants.homeRoute,
        title: 'Visa by Masters',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MainPage());
  }
}
