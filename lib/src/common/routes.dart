import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/applicationform/applicationform.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/mainpage.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visabycountry/visabycountry.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Constants.homeRoute:
        return MaterialPageRoute(builder: (context) => MainPage());
      case Constants.visaPage:
        var countryData = settings.arguments as dynamic;
        return MaterialPageRoute(
            builder: (context) => VisaCountry(countryData));
      case Constants.applyNow:
        return MaterialPageRoute(builder: (context) => ApplicationForm());
      default:
        return MaterialPageRoute(
            builder: (context) => Scaffold(
                  body: Center(
                    child: Text('No Route defined for ${settings.name}'),
                  ),
                ));
    }
  }
}
