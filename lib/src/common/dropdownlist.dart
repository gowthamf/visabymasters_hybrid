import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';

class DropDownList extends StatefulWidget {
  final Map<dynamic, String> dynamicDropDownList;
  final bool isCountryTo;
  final String hintText;
  final Color textColor;
  final Color dropDownTextColor;
  final String disablehint;

  DropDownList(this.dynamicDropDownList, this.hintText, this.textColor,
      this.dropDownTextColor, this.disablehint,
      {this.isCountryTo});
  @override
  State<StatefulWidget> createState() {
    return _DropDownList();
  }
}

class _DropDownList extends State<DropDownList> {
  dynamic selectValue;
  Map<dynamic, String> dynamicDropDownList1;

  @override
  void initState() {
    super.initState();
    dynamicDropDownList1 = widget.dynamicDropDownList;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.transparent,
        child: DropdownButton<dynamic>(
            disabledHint: Text('${widget.disablehint}'),
            iconSize: 30.0,
            style: TextStyle(
              color: Colors.black,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
            hint: Text(
              widget.hintText,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            value: selectValue,
            onChanged: (dynamic newValue) {
              setState(() {
                selectValue = newValue;
                if (widget.isCountryTo) {
                  Navigator.pushNamed(context, Constants.visaPage,
                      arguments: selectValue);
                }
              });
            },
            items: dynamicDropDownList1.entries
                .map<DropdownMenuItem<String>>((MapEntry<dynamic, String> e) {
              return DropdownMenuItem<String>(
                value: e.key,
                child: Container(
                  child: Text(
                    e.value,
                    style: TextStyle(
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  width: 200,
                ),
              );
            }).toList()));
  }
}
