import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'dart:html';

import 'package:visa_by_masters_hybrid/src/constants/constants.dart';

class AnimationImage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AnimationImage();
  }
}

class _AnimationImage extends State<AnimationImage> {
  // reader.onerror = (evt) => print("error ${reader.error.code}");
  @override
  Widget build(BuildContext context) {
    ui.platformViewRegistry.registerViewFactory(
        'animation-Image-html',
        (int viewId) => ImageElement(
            src:
                'assets/images/tourism.gif',
            width: (Constants.width).toInt(),
            height: (Constants.imageHeight).toInt()));
    return SizedBox(
      child: HtmlElementView(
        viewType: 'animation-Image-html',
      ),
    );
  }
}
