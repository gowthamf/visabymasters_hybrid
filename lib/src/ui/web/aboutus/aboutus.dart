import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';

class AboutUs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: Constants.width,
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  child: Image.asset(
                    'images/travel2.png',
                  ),
                ),
                Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.only(top: 10, bottom: 25),
                      child: Text(
                        'About Us',
                        style: TextStyle(fontSize: 25),
                      ),
                    ),
                  ],
                ),
                Card(
                  color: Colors.white54,
                  margin:
                      EdgeInsets.only(top: 50, left: 50, right: 50, bottom: 50),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          'The revolution:- It was time for a improve in the Travel and Visa service Industry. People love to travel, but we can'
                          't afford to waste time on process. ',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          'Visa by Masters.com is proud to introduce the advanced visa processing system to our expanding list of specialized services. We are able to serve for visas on behalf of the traveler, be it Visitor, Business and selective countries Student Visas.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          'Visa by Masters.com provides the simplest solution for processing your visa requirements. So, happily say goodbye to endless manual works. We enable travelers to process visas from a all electronic devices, such as; computer, smart phone, or tablet. We gladly welcome all Travel agents, destination management companies and corporate entities to use Visa by Masters.com to process any travel visa. We provide the best solution in the market.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10),
                        child: Text(
                          'We eliminate the frustration with complex bureaucratic websites, lengthy forms, and endless finding visa requirements. No frustration, wherever you are traveling, our world-class visa requirements tool allows you to check if you need a visa in seconds. Also, it will provide updated visa requirements accordingly. Our routinely updated database means you are a click away from knowing how to obtain your visa.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● Dedicated Local Team.  A highly personalized team devoted to managing your request from start to finish.',
                          softWrap: true,
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● One Contact Initiation.  Call 11111111119, email info@visabymasters.com, or complete your online order to initiate your service request. One of our dedicated specialists will contact you within the day.',
                          softWrap: true,
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● Document Preparation.  Assist with acquiring and completing all relevant documentation when applicable.',
                          softWrap: true,
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● Digital Photos, Visa Scan.  Save time by submitting your photos digitally. We do provide digital photo facilities with embassy specifications.',
                          softWrap: true,
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● Priority Submission.  By selecting Priority Service, your request will be treated with the highest priority.',
                          softWrap: true,
                        ),
                      ),
                      Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5),
                        child: Text(
                          '● Tailored Updates.  Stay apprised of every step in the process via your preferred method of contact - phone, email or text.',
                          softWrap: true,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            )
          ],
        ));
  }
}
