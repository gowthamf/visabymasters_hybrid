import 'package:flutter/material.dart';

class Security extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      margin: EdgeInsets.only(top: 50),
      height: 300,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 25),
                  child: Icon(
                    Icons.security,
                    size: 75,
                  )),
              Text(
                'SECURITY',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'World-class data centers and state-of-the-art methods for securing user accounts and information. Your credit card info will never be exposed to any government websites.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
