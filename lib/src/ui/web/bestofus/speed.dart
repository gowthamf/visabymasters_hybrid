import 'package:flutter/material.dart';

class Speed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 250,
      margin: EdgeInsets.only(top: 50),
      height: 300,
      alignment: Alignment.center,
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 25),
                  child: Icon(
                    Icons.alarm,
                    size: 75,
                  )),
              Text(
                'SPEED AND SIMPLICITY',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: Text(
                  'Easy, traveler-friendly application process. Usually 100% online and with clear instructions - much less complicated than dealing with foreign governments.',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
