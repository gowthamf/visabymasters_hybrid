import 'package:flutter/material.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visa_by_masters_hybrid/src/common/dropdownlist.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/statecontainer/statecontainer.dart';

class CheckVisa extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CheckVisa();
  }
}

class _CheckVisa extends State<CheckVisa>
    with AutomaticKeepAliveClientMixin, WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    bloc.getCountries();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void updateLoading(BuildContext context) {}

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print("Current state = $state");
  }

  @override
  // TODO: implement mounted
  bool get mounted => super.mounted;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return StreamBuilder(
      stream: bloc.countryList,
      builder: (context, AsyncSnapshot<List<Country>> snapshot) {
        if (snapshot.hasData) {
          return Container(
              child: Stack(
            children: <Widget>[
              Container(
                color: Colors.transparent,
                width: Constants.width,
                height: 400,
                child: Opacity(
                  opacity: 0.65,
                  child: Image.asset(
                    'images/banner.jpg',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Column(children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Check your Visa Requirements',
                      style: TextStyle(fontSize: 35),
                    ),
                  ],
                ),
                Container(
                    width: 300,
                    margin: EdgeInsets.all(25.0),
                    child: Divider(
                      color: Colors.black,
                      height: 10.0,
                    )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.only(left: 250),
                              width: 100,
                              height: 100,
                              child: Image.asset(
                                'images/passport.png',
                                alignment: Alignment.center,
                              )),
                          Container(
                            padding: EdgeInsets.only(left: 200),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'Where am I From?',
                                  style: TextStyle(
                                      fontSize: 30.0, color: Colors.black),
                                ),
                                Text(
                                  ' NATIONALITY AS IN PASSPORT',
                                  style: TextStyle(
                                      fontSize: 15.0, color: Colors.black),
                                ),
                                buildDropDown(false, snapshot.data,
                                    Colors.black, Colors.black),
                              ],
                            ),
                          )
                        ],
                      ),
                      height: 250.0,
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 50),
                            width: 100,
                            height: 100,
                          ),
                          Text(
                            'Where am I Going?',
                            style:
                                TextStyle(fontSize: 30.0, color: Colors.black),
                          ),
                          Text(
                            ' TRAVELLING TO',
                            style:
                                TextStyle(fontSize: 15.0, color: Colors.black),
                          ),
                          buildDropDown(
                              true, snapshot.data, Colors.white, Colors.black),
                        ],
                      ),
                      height: 250.0,
                    ),
                  ],
                ),
              ])
            ],
          ));
        }
        return Container();
      },
    );
  }

  Widget buildDropDown(bool isCountryTo, List<Country> snapshot,
      Color textColor, Color dropDownListColor) {
    Map<dynamic, String> dropDownLists = Map();
    snapshot.forEach(
        (c) => dropDownLists.addAll({'${c.countryId}': c.countryName}));
    return DropDownList(
      dropDownLists,
      'Select Country',
      textColor,
      dropDownListColor,
      'No Country Available',
      isCountryTo: isCountryTo,
    );
  }

  @override
  bool get wantKeepAlive => true;
}
