import 'package:flutter/material.dart';
import 'package:visa_by_masters_common/visa_by_masters_common.dart';
import 'package:visa_by_masters_hybrid/src/common/expansionpanel.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';

import 'package:visa_by_masters_hybrid/src/ui/web/popular-visa/countrytemplate.dart';

class Item {
  Item({
    this.expandedValue,
    this.headerValue,
    this.isExpanded = false,
  });

  Column expandedValue;
  Column headerValue;
  bool isExpanded;
}

List<Item> generateItems(int numberOfItems, List<Country> countryList,
    {BuildContext context}) {
  List<Map<dynamic, dynamic>> _countries = [];
  countryList.forEach((c) => _countries.add({
        'countrycode': c.countryId,
        'countryname': c.countryName,
        'image': c.countryFlag
      }));

  // Map<String, String> start = {
  //   'image': 'images/countries/aus.png',
  //   'countryname': 'Australia',
  //   'countrycode': '1'
  // };
  // Map<String, String> start1 = {
  //   'image': 'images/countries/br.png',
  //   'countryname': 'Brazil',
  //   'countrycode': '2'
  // };
  // Map<String, String> start2 = {
  //   'image': 'images/countries/jap.png',
  //   'countryname': 'Japan',
  //   'countrycode': '3'
  // };
  // Map<String, String> start3 = {
  //   'image': 'images/countries/nz.png',
  //   'countryname': 'New Zealand',
  //   'countrycode': '4'
  // };
  // Map<String, String> start4 = {
  //   'image': 'images/countries/uk.png',
  //   'countryname': 'United Kingdom',
  //   'countrycode': '5'
  // };
  // Map<String, String> start5 = {
  //   'image': 'images/countries/usa.png',
  //   'countryname': 'United States',
  //   'countrycode': '6'
  // };

  // _countries1.add(start);
  // _countries1.add(start1);
  // _countries1.add(start2);
  // _countries1.add(start3);
  // _countries1.add(start4);
  // _countries1.add(start5);

  return List.generate(numberOfItems, (int index) {
    return Item(
      headerValue: Column(
        children: <Widget>[
          Text(
            'Most Popular Visas',
            style: TextStyle(fontSize: 35),
          ),
          Container(
              width: 150,
              margin: EdgeInsets.all(25.0),
              child: Divider(
                color: Colors.black,
                height: 10.0,
              )),
          SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Scrollbar(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SizedBox(
                      height: 250,
                      child: CountryTemplate(_countries),
                    ),
                  ],
                ),
              ))
        ],
      ),
      expandedValue: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 250,
                    child: CountryTemplate(_countries),
                  ),
                ],
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 250,
                    child: CountryTemplate(_countries),
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  });
}

class PopularVisaStateFul extends StatefulWidget {
  PopularVisaStateFul({Key key}) : super(key: key);

  @override
  _PopularVisaState createState() => _PopularVisaState();
}

class _PopularVisaState extends State<PopularVisaStateFul> {
  static List<Item> _data;
  bool expanded = false;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: bloc.countryList,
      builder: (context, AsyncSnapshot<List<Country>> snapshot) {
        if (snapshot.hasData) {
          _data = generateItems(1, snapshot.data);
          return SingleChildScrollView(
            child: Container(
              child: _buildPanel(),
            ),
          );
        }
        return CircularProgressIndicator();
      },
    );
  }

  Widget _buildPanel() {
    return ExpansionPanelListC(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          expanded = !isExpanded;
        });
      },
      children: _data.map<ExpansionPanelC>((Item item) {
        item.isExpanded = expanded;
        return ExpansionPanelC(
          headerBuilder: (BuildContext context, bool isExpanded) {
            return ListTile(
              title: item.headerValue,
            );
          },
          body: ListTile(
            title: item.expandedValue,
          ),
          isExpanded: item.isExpanded,
        );
      }).toList(),
    );
  }
}
