import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visa-types/countervisa.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visa-types/electronicvisa.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visa-types/onarrival.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visa-types/personvisa.dart';

class VisaTypes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          DefaultTabController(
            initialIndex: 0,
            length: 4,
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 150.0, right: 150.0),
                  child: TabBar(
                    labelPadding: EdgeInsets.only(right: 10.0, left: 10.0),
                    tabs: <Widget>[
                      Tab(
                        child: Center(
                          widthFactor: 50.0,
                          child: Text(
                            'Electronic Visa',
                            style:
                                TextStyle(color: Colors.black, fontSize: 20.0),
                          ),
                        ),
                      ),
                      Tab(
                        child: Center(
                          child: Text(
                            'Visa on Arrival',
                            style:
                                TextStyle(color: Colors.black, fontSize: 20.0),
                          ),
                        ),
                      ),
                      Tab(
                        child: Center(
                          child: Text(
                            'Paper Visa by Counter',
                            style:
                                TextStyle(color: Colors.black, fontSize: 20.0),
                          ),
                        ),
                      ),
                      Tab(
                        child: Center(
                          child: Text(
                            'Paper Visa(In-Person)',
                            style:
                                TextStyle(color: Colors.black, fontSize: 20.0),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  height: 450,
                  child: TabBarView(
                    children: [
                      ElectronicVisa(),
                      VisaOnArrival(),
                      PaperVisaByCounter(),
                      PaperVisaInPerson()
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
