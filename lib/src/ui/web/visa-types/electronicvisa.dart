import 'package:flutter/material.dart';

class ElectronicVisa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 50, horizontal: 50),
      child: Column(
        children: <Widget>[
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Card(
                        elevation: 5.0,
                        shape: CircleBorder(
                            side: BorderSide(width: 10.0, color: Colors.grey)),
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 50),
                              width: 200,
                              height: 200,
                              decoration: BoxDecoration(
                                  border: Border(
                                    right: BorderSide(
                                        color: Colors.lightGreen, width: 3.0),
                                  ),
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                    image: AssetImage('images/step1.png'),
                                    fit: BoxFit.fill,
                                  )),
                            ),
                          ],
                        )),
                    Text(
                      'FILL OUT OUR ONLINE APPLICATION',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      width: 250,
                      child: Text(
                        'Complete our easy online application and pay with credit card',
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 50),
                    width: 100,
                    child: Image.asset(
                      'images/black_arrow.png',
                      height: 50,
                    )),
                Column(
                  children: <Widget>[
                    Card(
                      elevation: 5.0,
                      shape: CircleBorder(
                          side: BorderSide(width: 10.0, color: Colors.grey)),
                      child: Container(
                        width: 200,
                        height: 200,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage('images/step2.png'),
                              fit: BoxFit.contain,
                            )),
                      ),
                    ),
                    Text(
                      'SEND THE DOCUMENTS VIA EMAIL TO US',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      width: 250,
                      child: Text(
                        'No need to deal with the embassy. We do it for you so you don'
                        't lose valuable time',
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
                Container(
                    margin: EdgeInsets.only(bottom: 50),
                    width: 100,
                    child: Image.asset(
                      'images/black_arrow.png',
                      height: 50,
                    )),
                Column(
                  children: <Widget>[
                    Card(
                      margin: EdgeInsets.only(top: 25),
                      elevation: 5.0,
                      shape: CircleBorder(
                          side: BorderSide(width: 10.0, color: Colors.grey)),
                      child: Container(
                        width: 200,
                        height: 200,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage('images/step3.png'),
                              fit: BoxFit.contain,
                            )),
                      ),
                    ),
                    Text(
                      'RECEIVE THE DECISION VIA EMAIL',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    Container(
                      width: 250,
                      child: Text(
                        'Present your Passport and the Document we provide upon entry to destination country',
                        style: TextStyle(fontSize: 15),
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
