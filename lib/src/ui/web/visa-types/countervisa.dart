import 'package:flutter/material.dart';

class PaperVisaByCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 50, horizontal: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Card(
                    elevation: 5.0,
                    shape: CircleBorder(
                        side: BorderSide(width: 10.0, color: Colors.grey)),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage('images/step1.png'),
                            fit: BoxFit.fill,
                          )),
                    ),
                  ),
                  Text(
                    'FILL OUT OUR ONLINE APPLICATION',
                    style: TextStyle(fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    width: 250,
                    child: Text(
                      'Complete our easy online application and pay with credit card',
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(bottom: 50),
                child: Image.asset(
                  'images/black_arrow.png',
                  height: 50,
                ),
              ),
              Column(
                children: <Widget>[
                  Card(
                    margin: EdgeInsets.only(left: 25),
                    elevation: 5.0,
                    shape: CircleBorder(
                        side: BorderSide(width: 10.0, color: Colors.grey)),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage('images/step4.png'),
                            fit: BoxFit.contain,
                          )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25, right: 25),
                    width: 250,
                    child: Text(
                      'SEND PASSPORT VIA OUR AUTHORIZED COURIER',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    width: 250,
                    child: Text(
                      'Need to discuss with Watson',
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(bottom: 50),
                child: Image.asset(
                  'images/black_arrow.png',
                  height: 50,
                ),
              ),
              Column(
                children: <Widget>[
                  Card(
                    margin: EdgeInsets.only(left: 25, top: 25),
                    elevation: 5.0,
                    shape: CircleBorder(
                        side: BorderSide(width: 10.0, color: Colors.grey)),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage('images/step5.png'),
                            fit: BoxFit.contain,
                          )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    width: 250,
                    child: Text(
                      'WE PROCESS VISA AT CONSULATE',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    width: 250,
                    child: Text(
                      'WE PRINT YOUR APPLICATION AND PROCESS YOUR VISA AT THE CONSULATE',
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(bottom: 50),
                  width: 50,
                  child: Image.asset(
                    'images/black_arrow.png',
                    height: 50,
                  )),
              Column(
                children: <Widget>[
                  Card(
                    margin: EdgeInsets.only(left: 25,bottom: 25),
                    elevation: 5.0,
                    shape: CircleBorder(
                        side: BorderSide(width: 10.0, color: Colors.grey)),
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage('images/step6.png'),
                            fit: BoxFit.contain,
                          )),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    width: 250,
                    child: Text(
                      'ENTER DESTINATION',
                      style: TextStyle(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 25),
                    width: 250,
                    child: Text(
                      'Discuss with watson',
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
