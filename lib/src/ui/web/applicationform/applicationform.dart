import 'package:flutter/material.dart';

class ApplicationForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ApplicationForm();
  }
}

class _ApplicationForm extends State<ApplicationForm> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Application Form'),
        ),
        body: Center(
          child: Container(
              margin: EdgeInsets.only(top: 100),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 50, right: 50),
                        width: 400,
                        child: TextFormField(
                          maxLengthEnforced: true,
                          maxLength: 50,
                          textCapitalization: TextCapitalization.words,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            filled: true,
                            icon: Icon(Icons.person),
                            hintText: 'Your First Name',
                            labelText: 'First Name',
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        width: 400,
                        child: TextField(
                          maxLengthEnforced: true,
                          maxLength: 50,
                          textCapitalization: TextCapitalization.words,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            filled: true,
                            hintText: 'Your Last Name',
                            labelText: 'Last Name',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 50, right: 50),
                        width: 400,
                        child: TextFormField(
                          maxLengthEnforced: true,
                          maxLength: 2,
                          textCapitalization: TextCapitalization.words,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            filled: true,
                            icon: Icon(Icons.phone),
                            hintText: 'How Old Are You?',
                            labelText: 'Age',
                          ),
                          keyboardType: TextInputType.number,
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        width: 400,
                        child: TextField(
                          maxLengthEnforced: true,
                          maxLength: 50,
                          textCapitalization: TextCapitalization.words,
                          decoration: const InputDecoration(
                            border: UnderlineInputBorder(),
                            filled: true,
                            hintText: 'Your Last Name',
                            labelText: 'Last Name',
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )),
        ));
  }
}
