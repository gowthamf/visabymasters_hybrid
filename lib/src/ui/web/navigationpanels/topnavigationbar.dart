import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/common/customdialog.dart';
import 'dart:html' as html;

import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contactus/contactus.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/homecontents.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/legalization.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/orderstatus.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/passport.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/servicedirectory.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/contentpages/visa.dart';

class NavigationBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NavigationBar();
  }
}

class _NavigationBar extends State<NavigationBar>
    with SingleTickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  static GlobalKey<_ContentPage> _key;
  TabController _tabController;
  TabController tabCo;
  ScrollController _scrollController;
  bool fixedScroll = false;
  bool isLoading = true;

  @override
  void initState() {
    _scrollController = ScrollController();
    _tabController = TabController(length: 6, vsync: this);
    _tabController.addListener(_smoothScrollToTop);
    _scrollController.addListener(_scrollListener);
    _key = GlobalKey<_ContentPage>();
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  _scrollListener() {}

  _smoothScrollToTop() {
    _scrollController.animateTo(
      0,
      duration: Duration(microseconds: 5000),
      curve: Curves.easeInToLinear,
    );
  }

  void changeTab(int pageIndex) {
    setState(() {
      _tabController.animateTo(pageIndex);
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: Scrollbar(
        child: NestedScrollView(
          controller: _scrollController,
          physics: AlwaysScrollableScrollPhysics(),
          headerSliverBuilder: (context, value) {
            return [
              SliverAppBar(
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Constants.applyNow);
                    },
                    child: Text('Apply Now'),
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: Text('About Us'),
                  ),
                  FlatButton(
                    onPressed: () {},
                    child: Text('Contact Us'),
                  )
                ],
                backgroundColor: Colors.white,
                title: Container(
                    child: Row(
                  children: <Widget>[
                    Container(
                      height: 50,
                      width: 50,
                      child: Image.asset('images/logo.png'),
                    ),
                    Text(
                      'Visa by Masters',
                      style: TextStyle(color: Colors.black, fontSize: 40),
                    )
                  ],
                )),
                pinned: true,
                floating: true,
                snap: false,
                bottom: TabBar(
                  onTap: (int i) {
                    _key.currentState._pageController.animateToPage(i,
                        duration: const Duration(milliseconds: 100),
                        curve: Curves.easeInOut);
                  },
                  indicatorColor: Colors.red,
                  indicatorWeight: 5.0,
                  controller: _tabController,
                  tabs: [
                    Tab(
                      child: Text(
                        'HOME',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'VISAS',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'PASSPORTS',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'LEGALIZATION',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'SERVICE DIRECTORY',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                    Tab(
                      child: Text(
                        'PROCESS STATUS',
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  ],
                ),
              ),
            ];
          },
          body: ContentsPage(
            key: _key,
            changeTabs: changeTab,
          ),
        ),
      ),
      floatingActionButton: Align(
        alignment: Alignment.bottomRight,
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FloatingActionButton(
                heroTag: 'tagPre',
                onPressed: () async {
                  await showGeneralDialog(
                      context: context,
                      barrierDismissible: true,
                      barrierLabel: 'Dismiss',
                      transitionDuration: Duration(milliseconds: 500),
                      transitionBuilder:
                          (context, animation, secondaryAnimation, child) {
                        var fadeTween = CurveTween(curve: Curves.fastOutSlowIn);
                        var fadeAnimation = fadeTween.animate(animation);
                        return FadeTransition(
                          opacity: fadeAnimation,
                          child: CusAlertDialog(
                              title: Text('Contact Us'), content: ContactUs()),
                        );
                      },
                      pageBuilder: (context, animation, secondaryAnimation) {});
                },
                child: Icon(Icons.chat),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ContentsPage extends StatefulWidget {
  final Function changeTabs;
  const ContentsPage({Key key, this.changeTabs}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _ContentPage();
  }
}

class _ContentPage extends State<ContentsPage>
    with AutomaticKeepAliveClientMixin {
  PageController _pageController;
  int pageIndex = 0;
  double loadHeight;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _changePage(int pageIndex) {
    _pageController.animateToPage(pageIndex,
        duration: const Duration(milliseconds: 500), curve: Curves.linear);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return PageView(
      controller: _pageController,
      onPageChanged: (int i) {
        setState(() {
          pageIndex = i;
          widget.changeTabs(pageIndex);
        });
      },
      physics: AlwaysScrollableScrollPhysics(),
      children: <Widget>[
        HomePageContents(),
        VisaPage(
          changeTab: _changePage,
        ),
        Passports(),
        Legalization(),
        ServiceDirectory(),
        OrderStatus(),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
