import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';

class BottomPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: Constants.width,
      height: 200,
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ButtonBar(
                alignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  FlatButton(
                    child: Text('TRACK YOUR ORDER'),
                    onPressed: () {},
                  ),
                  FlatButton(
                    child: Text('SERVICES-VISAS'),
                    onPressed: () {},
                  ),
                  FlatButton(
                    child: Text('APPLY NOW'),
                    onPressed: () {},
                  ),
                  FlatButton(
                    child: Text('EMBASSY LISTINGS'),
                    onPressed: () {},
                  )
                ],
              )
            ],
          ),
          Flexible(
            flex: 3,
            child: Container(
              color: Color(0xff12283a),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonBar(
                    children: <Widget>[
                      FlatButton(
                        child: Text('ABOUT US',
                            style: TextStyle(color: Colors.white)),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: Text('PARTERS / AFFILLIATES',
                            style: TextStyle(color: Colors.white)),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child: Text('PRIVACY',
                            style: TextStyle(color: Colors.white)),
                        onPressed: () {},
                      ),
                      FlatButton(
                        child:
                            Text('FAQ', style: TextStyle(color: Colors.white)),
                        onPressed: () {},
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          Flexible(
            flex: 3,
            child: Container(
              height: 300,
              color: Color(0xff12283a),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 50),
                    child: SizedBox(
                      width: 500,
                      child: Text(
                        '© 2019 visabymasters.com. All rights reserved. Visabymasters and Visabymasters logo are registered trademarks of visabymasters.com.',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  Container(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Connect with us:',
                        style: TextStyle(color: Colors.white),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Image.asset('images/facebook-box.png'),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Image.asset('images/twitter.png'),
                      ),
                      IconButton(
                        onPressed: () {},
                        icon: Image.asset('images/linkedin-box.png'),
                      ),
                    ],
                  ))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
