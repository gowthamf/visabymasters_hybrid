import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/common/responsivelayout.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/navigationpanels/topnavigationbar.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: NavigationBar(),
    );
  }
}
