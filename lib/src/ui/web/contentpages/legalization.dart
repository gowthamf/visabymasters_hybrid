import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/bestofus/performace.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/navigationpanels/bottompanel.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/testimonials/testimonials.dart';

class Legalization extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Legalization();
  }
}

class _Legalization extends State<Legalization> {
  bool _visible = true;
  Timer _timer;
  bool isShowing = true;

  @override
  void initState() {
    super.initState();
    _animated();
  }

  void _animated() {
    _timer = Timer(Duration(milliseconds: 400), () {
      setState(() {
        _visible = false;
      });
    });
  }

  void showTestimonial(PointerSignalEvent _) =>
      setState(() => isShowing = false);

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return AnimatedOpacity(
            opacity: _visible ? 0.0 : 1.0,
            duration: const Duration(milliseconds: 500),
            child: Column(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'We offer fast, easy, and secure document legalization, translation, notarization. Start your order below or contact us at info@visabymasters.com to begin a consultation. We will take the time to understand your legalization needs and work with you step-by-step to ensure you get the right documents in a timely fashion. Please note the final price for your request will be determined by the type of documents required and how quickly you need them.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: FlatButton(
                          child: Text('Start Order'),
                          onPressed: () {},
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 10.0),
                        child: Text(
                          'Common documents that may require legalization are:',
                          softWrap: true,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 25.0),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5.0, top: 15.0),
                        child: Text(
                          '•	Academic Certificates ',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5.0, top: 15.0),
                        child: Text(
                          '•	Birth, Marriage, and Divorce Certificates',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5.0, top: 15.0),
                        child: Text(
                          '•	Business Contracts',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5.0, top: 15.0),
                        child: Text(
                          '•	Certificate of No Impediment',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 5.0, top: 15.0),
                        child: Text(
                          '•	Religious Documents',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 15.0, top: 15.0),
                        child: Text(
                          'Legalization is a multi-step process. Often your document must be legalized by a local or Embassy Italy of the destination country to be considered genuine and valid.',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 15.0, top: 15.0),
                        child: Text(
                          'We can help. Contact our legalization experts at info@visabymasters.com to begin a consultation. We will take the time to understand your legalization needs and work with you step-by-step to ensure you get the right documents in a timely fashion.',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 15.0, top: 15.0),
                        child: Text(
                          'Translations: In some cases, governments will require that documents be translated. We can translate your documents for use by the appropriate embassy or government agency. We can also translate foreign documents into English.',
                          softWrap: true,
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 15.0, top: 15.0),
                        child: Text(
                          'Notarization: Notarization verifies the authenticity of facts set out in a document and countries often require notarization. We provide notarization services to assure that your important documents are considered valid and legal.',
                          softWrap: true,
                        )),
                    Listener(
                      onPointerSignal: showTestimonial,
                      child: AnimatedOpacity(
                          opacity: isShowing ? 0.0 : 1.0,
                          duration: const Duration(milliseconds: 500),
                          child: Card(
                            child: Testimonials(),
                          )),
                    ),
                    Listener(
                      onPointerSignal: showTestimonial,
                      child: AnimatedOpacity(
                        opacity: isShowing ? 0.0 : 1.0,
                        duration: const Duration(milliseconds: 500),
                        child: Card(
                          child: PerformanceTemplate(),
                        ),
                      ),
                    ),
                    Card(
                      child: BottomPanel(),
                    )
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
