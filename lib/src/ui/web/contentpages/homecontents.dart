import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/common/animationImage.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/bestofus/performace.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/checkvisa/checkvisa.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/navigationpanels/bottompanel.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/popular-visa/popularvisa.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/testimonials/testimonials.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/visa-types/visatypes.dart';


class HomePageContents extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageContents();
  }
}

class _HomePageContents extends State<HomePageContents>
    with AutomaticKeepAliveClientMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Constants.width = MediaQuery.of(context).size.width;
    Constants.height = MediaQuery.of(context).size.height;
    return Container(
      child: ListView(
        physics: const ClampingScrollPhysics(),
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: Constants.width,
                height: Constants.imageHeight,
                child: AnimationImage(),
              ),
              Card(
                child: CheckVisa(),
              ),
              Card(
                child: PopularVisaStateFul(),
              ),
              Card(
                child: VisaTypes(),
              ),
              Card(
                child: Testimonials(),
              ),
              Card(
                child: PerformanceTemplate(),
              ),
              Card(
                child: BottomPanel(),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
