import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:visa_by_masters_hybrid/src/constants/constants.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/bestofus/performace.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/navigationpanels/bottompanel.dart';
import 'package:visa_by_masters_hybrid/src/ui/web/testimonials/testimonials.dart';

class ServiceDirectory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ServiceDirectory();
  }
}

class _ServiceDirectory extends State<ServiceDirectory> {
  bool _visible = true;
  Timer _timer;
  bool isTestimonial = true;

  @override
  void initState() {
    super.initState();
    _animated();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  void _animated() {
    _timer = Timer(Duration(milliseconds: 400), () {
      setState(() {
        _visible = false;
      });
    });
  }

  void showTestimonial(PointerSignalEvent _) =>
      setState(() => isTestimonial = false);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        physics: const ClampingScrollPhysics(),
        itemCount: 1,
        itemBuilder: (context, index) {
          return AnimatedOpacity(
            opacity: _visible ? 0.0 : 1.0,
            duration: const Duration(milliseconds: 500),
            child: Column(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Value Added Services',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Digital Photo Service',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Appointment Scheduling Visa Submission',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Online Visa Form Filling',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Digital Photo Service',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Comprehensive advice on supporting document preparation for visa submission',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Draft Visa Request Letters Preparation',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Document Translation',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                        child: Text(
                          '•	Notarization Services',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Visas Documents Delivery Methods',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Our International & Local Courier Partner - DTDC Srilanka',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Global Parcel Delivery, Sri Lanka is the local representative of DPD Group, Europe'
                          's 2nd largest express company and DTDC Express Ltd.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Saturday Delivery',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Container(
                        width: Constants.width,
                        padding: EdgeInsets.only(bottom: 25.0, top: 15.0),
                        child: Text(
                          'Need your documents by Saturday?  Visa by Masters- can arrange to have your documents delivered to you by 8:30 am on a Saturday.  This service ensures that your documents reach you in time for your next trip.  Contact Visa by Masters-  Customer Care Center at 11111111111 or info@visabymasters.com for further details.',
                          softWrap: true,
                          style: TextStyle(fontSize: 15),
                        )),
                    Listener(
                      onPointerSignal: showTestimonial,
                      child: AnimatedOpacity(
                          opacity: isTestimonial ? 0.0 : 1.0,
                          duration: const Duration(milliseconds: 500),
                          child: Card(
                            child: Testimonials(),
                          )),
                    ),
                    Listener(
                      onPointerSignal: showTestimonial,
                      child: AnimatedOpacity(
                        opacity: isTestimonial ? 0.0 : 1.0,
                        duration: const Duration(milliseconds: 500),
                        child: Card(
                          child: PerformanceTemplate(),
                        ),
                      ),
                    ),
                    Card(
                      child: BottomPanel(),
                    )
                  ],
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
