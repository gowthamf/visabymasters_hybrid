import 'dart:async' show Future;
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class Constants {
  static double width;
  static double imageHeight = width / 3.5;
  static double height;
  bool isLoading = true;
  static double loadingHeight = 5;
  static String baseUrl;
  static const String homeRoute = '/';
  static const String visaPage = '/visacountry';
  static const String applyNow = '/applynow';
  static String imageBaseUrl;
}
